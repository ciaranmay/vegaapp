using Microsoft.EntityFrameworkCore;
using vega2.Models;

namespace vega2.Persistance
{
    public class VegaDbContext : DbContext
    {
        public VegaDbContext(DbContextOptions<VegaDbContext> options): base(options)
        {
            //System.Configuration.ConfigurationManager
            //Register this class as a service for dependency injection so later when create a controller to pass dbContext in constructor of controller will auto create dbContext and pass to controller

        }
        public DbSet<Make> Makes { get; set; }
    }
}