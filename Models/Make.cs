using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega2.Models
{
    public class Make
    {

        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public ICollection<Model> Models { get; set; }// list of models to be populated by the db.....ctrl + . to add namespace on top of file


        public Make()
        {

            this.Models = new Collection<Model>();// init the list so no null ref exception 
        }
    }
}
