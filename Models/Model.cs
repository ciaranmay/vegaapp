
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega2.Models
{
    [Table("Models")]
    public class Model
    {
        public int Id { get; set; }
         [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public Make Make { get; set; }// nav property
        public int MakeId { get; set; }// foreign key to assioate model with the make
    }
}