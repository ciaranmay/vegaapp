using System.Collections.Generic;
using System.Collections.ObjectModel;

using vega2.Models;

namespace vega2.Controllers.Resources
{
    public class MakeResource
    {
         public int Id { get; set; }
       
        public string Name { get; set; }

        public ICollection<ModelResource> Models { get; set; }// list of models to be populated by the db.....ctrl + . to add namespace on top of file


        public MakeResource()
        {

            this.Models = new Collection<ModelResource>();// init the list so no null ref exception 
        }
    }
}